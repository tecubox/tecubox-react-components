import React from 'react';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';

import TabPanels from './index';

const tabs = [
  {
    name: 'summary',
    label: 'Summary',
    component: ({onInitialize}) => {onInitialize(); return (<div>Summary component</div>); },
    initialize: (active) => { if(active) console.log('initialize summary'); },
  },
  {
    name: 'worlds',
    label: 'Worlds',
    component: ({onInitialize}) => {onInitialize(); return (<div>Worlds component</div>); },
    initialize: (active) => { if(active) console.log('initialize world'); },
  },
  {
    name: 'themes',
    label: 'Themes',
    component: ({onInitialize}) => {onInitialize(); return (<div>Themes component</div>); },
    initialize: (active) => { if(active) console.log('initialize themes'); },
  }
];

storiesOf('Tab Panels', module)
  .add('Default', () =>
    <TabPanels tabs={tabs} currentTab={'summary'} headerTitle={'Tecubox'} onTabClick={action('tab-click')}/>)
  .add('Worlds selected', () =>
    <TabPanels tabs={tabs} currentTab={'worlds'} headerTitle={'Tecubox'} onTabClick={action('tab-click')}/>)
  .add('Themes selected', () =>
    <TabPanels tabs={tabs} currentTab={'themes'} headerTitle={'Tecubox'} onTabClick={action('tab-click')}/>);
