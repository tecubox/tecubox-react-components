import React from 'react';
import {Menu} from 'semantic-ui-react';
import ToggleDisplay from 'react-toggle-display';

const TabPanels = (props) => {
  const {tabs, currentTab, onTabClick, headerTitle, ...otherProps} = props;
  const tabItems = tabs.map(tabInfo => {
    const {name, label} = tabInfo;
    return (<Menu.Item
      key={name}
      name={name}
      label={label}
      active={currentTab === name}
      onClick={() => onTabClick(name)}
    />);
  });
  const tabPanels = tabs.map(tabInfo => {
    const {name, initialize, component: TabComponent} = tabInfo;
    return (
      <ToggleDisplay key={name} show={currentTab === name}>
        <TabComponent onInitialize={() => initialize(currentTab === name)}/>
      </ToggleDisplay>
    );
  });
  return (
    <div>
      <Menu>
        <Menu.Item header>{headerTitle}</Menu.Item>
        {tabItems}
      </Menu>
      {tabPanels}
    </div>
  );
};

export default TabPanels;
