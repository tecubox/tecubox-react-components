import TabPanels from 'navigation/tab-panels';
import apiMiddleware from 'middleware/api';

export {TabPanels, apiMiddleware};
