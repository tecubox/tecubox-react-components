# Tecubox React Components

These are a set of React Components that can be included in interfaces used by the Tecubox systems. This way we don't
have to redevelop them for each project. It will include common use cases like navigation, graph and map displays, etc.
They will use [Semantic UI] under the covers.

